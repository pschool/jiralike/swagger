/*
 * Coordiantion front back
 *
 * School project
 *
 * API version: 1.0.0
 * Contact: paul.tedesco@live.fr
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */
package swagger

type User struct {

	Id string `json:"id,omitempty"`

	CreatedAt string `json:"created_at,omitempty"`

	UpdatedAt string `json:"updated_at,omitempty"`

	Name string `json:"name"`

	FirstName string `json:"first_name"`

	Enterprise string `json:"enterprise"`

	Siret string `json:"siret,omitempty"`

	Email string `json:"email"`

	Phone string `json:"phone,omitempty"`

	StatusEnt string `json:"status_ent,omitempty"`

	Profil string `json:"profil,omitempty"`

	Password string `json:"password"`
}
